﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW22_23
{
    public class Employee
    {
        public string FirstName { get; private set; }
        public string SecondName { get; private set; }
        public DateTime Birthday { get; private set; }
        public double SalaryAmount { get; private set; }
        public Position Pos { get; private set; }

        public static Employee RandEmployee()
        {
            var r = new Random();
            List<string> FNM = new List<string>()
            {
                "Карим     ",
                "Леонард   ",
                "Юлиан     ",
                "Николай   ",
                "Август    ",
                "Матвей    ",
                "Илларион  ",
                "Лоренс    ",
                "Влад      ",
                "Кирилл    ",
                "Феликс    ",
                "Геннадий  ",
                "Ефрем     ",
                "Аполлон   ",
                "Борис     ",
                "Клим      ",
                "Юрий      ",
                "Марк      ",
                "Дарий     ",
                "Валерий   ",
                "Алексей   ",
            };
            List<string> SNM = new List<string>()
            {
                "Богданов     ",
                "Борисов      ",
                "Денисов      ",
                "Зиновьев     ",
                "Исаков       ",
                "Козлов       ",
                "Котовский    ",
                "Логинов      ",
                "Маслов       ",
                "Миклашевский ",
                "Муравьёв     ",
                "Негода       ",
                "Палий        ",
                "Пахомов      ",
                "Романенко    ",
                "Тарасюк      ",
                "Третьяков    ",
                "Устинов      ",
                "Филатов      ",
                "Шаров        ",
                "Щербаков     ",
                "Щукин        ",
                "Яковенко     ",
            };
            List<string> FNW = new List<string>()
            {
                "Алла      ",
                "Альбина   ",
                "Анфиса    ",
                "Валерия   ",
                "Вероника  ",
                "Елизавета ",
                "Искра     ",
                "Клавдия   ",
                "Клементина",
                "Ксения    ",
                "Люся      ",
                "Нелли     ",
                "Нина      ",
                "Ольга     ",
                "Рада      ",
                "Раиса     ",
                "Тамара    ",
                "Эмма      ",
                "Яна       ",                
            };
            List<string> SNW = new List<string>()
            {
                "Гайчук       ",
                "Гордеева     ",
                "Григорьева   ",
                "Дмитриева    ",
                "Дроздова     ",
                "Елисеева     ",
                "Карпова      ",
                "Кличко       ",
                "Князева      ",
                "Колесник     ",
                "Кравченко    ",
                "Куликова     ",
                "Лаврентьева  ",
                "Марочко      ",
                "Моисеенко    ",
                "Некрасова    ",
                "Передрий     ",
                "Петрик       ",
                "Потапова     ",
                "Силина       ",
                "Симоненко    ",
                "Тарасова     ",
                "Хованска     ",
                "Якушева      ",    
            };

            Employee employee = new Employee();

            if (r.Next(1,4)!=1)
            {
                employee.FirstName = FNM[r.Next(FNM.Count)];
                employee.SecondName = SNM[r.Next(SNM.Count)];
            }
            else
            {
                employee.FirstName = FNM[r.Next(FNM.Count)];
                employee.SecondName = SNM[r.Next(SNM.Count)];
            }

            employee.Birthday = DateTime.Today.AddDays(-r.Next(7000, 30000));
            int temp = r.Next(1, 6);
            employee.Pos = (Position)temp;
            employee.SalaryAmount = temp * r.Next(500, 1000);

            return employee;
        }

        public static Employee CreateEmployee()
        {
            Employee employee = new Employee();
            Console.WriteLine("Enter first name");
            employee.FirstName = Console.ReadLine();
            Console.WriteLine("Enter last name");
            employee.SecondName = Console.ReadLine();
            Console.WriteLine("Enter date of birth(example 22-10-1988)");
            employee.Birthday = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Enter position\n1. Junior\n2. Middle\n3. Senior\n4. Architect\n5. Manager\n6. Director");
            employee.Pos = (Position)Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter salary amount");
            employee.SalaryAmount = Convert.ToInt32(Console.ReadLine());
            return employee;
        }
               
        public override string ToString()
        {
            return $"{FirstName}\t{SecondName}\t{Birthday.ToShortDateString()}\t{SalaryAmount }\t{Pos}";
        }
    }
}
