﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW22_23
{
    public class Presenter
    {
        IView<Employee> _view = new View<Employee>();
        ICollectionManager<Employee> _collection = new CollectionManager<Employee>();

        public void Start(List<Employee> employees)
        {
            _collection.GetCollection(employees);
            _view.StartView += _collection.GetPage;
            _view.NextPage += _collection.NextPage;
            _view.OpenPageByNumber += _collection.OpenPageByNumber;
            _view.PreviousPage += _collection.PreviousPage;
            _view.Add += _collection.Add;
            _view.Remove += _collection.Remove;
            _view.FilterSorting += _collection.FilterSorting;
            _view.Start();
        }
    }
}
