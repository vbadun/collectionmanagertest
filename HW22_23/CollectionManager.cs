﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW22_23
{
    public class CollectionManager<Telement> : IEnumerable<Telement>, ICollectionManager<Telement>
        where Telement : class, new()
    {
        private const int TAKE = 5;
        private int _page = 0;
        private Func<Telement, bool> _predicateWhere;
        private Func<Telement, dynamic> _keySelector;
        private bool _reverse = false;

        private List<Telement> _collection;

        public CollectionManager()
        {

        }

        public CollectionManager(List<Telement> collection)
        {
            _collection = collection;

        }

        public void GetCollection(List<Telement> tcollection)
        {
            _collection = tcollection;
        }

        public List<Telement> GetPage()
        {
            return FilterSorting();
        }

        public List<Telement> NextPage()
        {
            if (_collection.Count >= _page * TAKE)
            {
                _page++;
            }
            return GetPage();
        }

        public List<Telement> PreviousPage()
        {
            if (_page > 0)
            {
                _page--;
            }
            return GetPage();
        }

        public List<Telement> OpenPageByNumber(int page)
        {
            if (page <= 0)
            {
                _page = 0;
            }
            else if (_collection.Count <= page * TAKE)
            {
                _page = (_collection.Count % TAKE) == 0 ? _collection.Count / TAKE -1 : _collection.Count / TAKE ;
            }
            else
            {
                _page = page;
            }
            return GetPage();
        }

        public List<Telement> Add(Telement element)
        {
            _collection.Add(element);
            return FilterSorting();
        }

        public List<Telement> Remove(Telement element)
        {
            _collection.Remove(element);
            return FilterSorting();
        }

        public List<Telement> FilterSorting(Func<Telement, bool> predicateWhere, Func<Telement, dynamic> keySelector, bool reverse = false)
        {
            _predicateWhere = predicateWhere;
            _keySelector = keySelector;
            _reverse = reverse;
            return FilterSorting();
        }

        private List<Telement> FilterSorting()
        {
            var coll = Clone();
            if (_predicateWhere != null)
            {
                coll = coll.Where(_predicateWhere).ToList();
            }
            if (!_reverse)
            {
                if (_keySelector != null )
                {         
                    coll = coll.OrderBy(_keySelector).ToList();
                }
            }
            else
            {
                if (_keySelector != null)
                {
                    coll = coll.OrderByDescending(_keySelector).ToList();
                }
            }

            return coll.Skip(_page * TAKE).Take(TAKE).ToList();
        }

        public IEnumerator<Telement> GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        private List<Telement> Clone()
        {          
            return ((CollectionManager<Telement>)MemberwiseClone())._collection;
        }
    }
}
