﻿using System;
using System.Collections.Generic;

namespace HW22_23
{
    public interface IView<Telement>
        where Telement : class, new()
    {
        event Func<List<Telement>> StartView;
        event Func<List<Telement>> NextPage;
        event Func<List<Telement>> PreviousPage;
        event Func<int, List<Telement>> OpenPageByNumber;
        event Func<Telement, List<Telement>> Add;
        event Func<Telement, List<Telement>> Remove;
        event Func<Func<Telement, bool>, Func<Telement, dynamic>, bool, List<Telement>> FilterSorting;

        void Start();
    }
}