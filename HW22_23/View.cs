﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW22_23
{
    public class View<Telement> : IView<Telement>
        where Telement : Employee, new()
    {

        public event Func<List<Telement>> StartView;
        public event Func<List<Telement>> NextPage;
        public event Func<List<Telement>> PreviousPage;
        public event Func<int, List<Telement>> OpenPageByNumber;
        public event Func<Telement, List<Telement>> Add;
        public event Func<Telement, List<Telement>> Remove;
        public event Func<Func<Telement, bool>, Func<Telement, dynamic>, bool, List<Telement>> FilterSorting;

        Func<Telement, bool> _predicateWhere;
        private Func<Telement, dynamic> _keySelector;
        private bool _reverse = false;

        public void Start()
        {
            if (StartView != null)
            {
                ShowCollection(StartView());
                int temp = -1;
                do
                {
                    temp = Dialog();

                    switch (temp)
                    {
                        case 1:
                            ShowCollection(PreviousPage());
                            break;
                        case 2:
                            ShowCollection(NextPage());
                            break;
                        case 3:
                            ShowOpenPageByNumber();
                            break;
                        case 4:
                            AddElement();                            
                            break;
                        case 5:
                            RemoveElement();
                            break;
                        case 6:
                            ShowFilterSorting();
                            break;
                    }
                } while (temp!=0);
            }
        }

        private void ShowFilterSorting()
        {
            Console.Clear();
            Console.WriteLine("Set filtering by parameter:");
            Console.WriteLine("1. Position\n2. SalaryAmount \n3. Age");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    FilteringPosition();  
                    break;
                case 2:
                    FilteringSalaryAmount();
                    break;
                case 3:
                    FilteringAge();
                    break;
            }
            Console.WriteLine("Set Sorting by parameter:");
            Console.WriteLine("1. Position\n2. SalaryAmount \n3. Age");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    _keySelector=x=>x.Pos;
                    break;
                case 2:
                    _keySelector = x => x.SalaryAmount;
                    break;
                case 3:
                    _keySelector = x => x.Birthday;
                    break;
            }
            Console.WriteLine("1. Ascending\n2. descending");
            if (Convert.ToInt32(Console.ReadLine())==2)
            {
                _reverse = true;
            }
            ShowCollection(FilterSorting(_predicateWhere, _keySelector, _reverse));




        }

        private void FilteringAge()
        {
            Console.WriteLine("Enter a age (in years)");
            int years = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"1. more {years} \n2.less {years}");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    _predicateWhere = x => x.Birthday.Year >= years;
                    break;
                case 2:
                    _predicateWhere = x => x.Birthday.Year <= years;
                    break;
            };
        }

        private void FilteringSalaryAmount()
        {
            Console.WriteLine("Enter a salary");
            int salary = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"1. more {salary} \n2.less {salary}");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    _predicateWhere = x => x.SalaryAmount >= salary;
                    break;
                case 2:
                    _predicateWhere = x => x.SalaryAmount <= salary;
                    break;
            }
        }

        private void FilteringPosition()
        {
            Console.WriteLine("Enter position\n1. Junior\n2. Middle\n3. Senior\n4. Architect\n5. Manager\n6. Director");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    _predicateWhere = x => x.Pos == Position.Junior;
                    break;
                case 2:
                    _predicateWhere = x => x.Pos == Position.Middle;
                    break;
                case 3:
                    _predicateWhere = x => x.Pos == Position.Senior;
                    break;
                case 4:
                    _predicateWhere = x => x.Pos == Position.Architect;
                    break;
                case 5:
                    _predicateWhere = x => x.Pos == Position.Manager;
                    break;
                case 6:
                    _predicateWhere = x => x.Pos == Position.Director;
                    break;
            }
        }

        private void RemoveElement()
        {
            dynamic element = Employee.CreateEmployee();            
            ShowCollection(Remove(element));
        }

        private void AddElement()
        {
            dynamic element = Employee.CreateEmployee();
            ShowCollection(Add(element));
        }

        private void ShowOpenPageByNumber()
        {
            Console.Clear();
            Console.WriteLine("Enter page number:");
            ShowCollection(OpenPageByNumber(Convert.ToInt32(Console.ReadLine())));
        }

        private void ShowCollection(List<Telement> ts)
        {
            if (ts!=null)
            {
                Console.Clear();
                foreach (var item in ts)
                {
                    Console.WriteLine(item);
                }
            }            
        }

        private int Dialog()
        {
            Console.WriteLine("\n1. PreviousPage \n2. NextPage \n3. OpenPageByNumber \n4. Add element \n5. Remove element \n6. FilterSorting\n0. Exit");
            return Convert.ToInt32(Console.ReadLine());
        }
    }
}
