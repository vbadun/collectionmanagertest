﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HW22_23
{
    public interface ICollectionManager<Telement>
        where Telement : class, new()
    {
        void GetCollection(List<Telement> tcollection);
        List<Telement> Add(Telement element);
        List<Telement> FilterSorting(Func<Telement, bool> predicateWhere, Func<Telement, dynamic> keySelector, bool reverse = false);
        List<Telement> GetPage();
        List<Telement> NextPage();
        List<Telement> OpenPageByNumber(int page);
        List<Telement> PreviousPage();
        List<Telement> Remove(Telement element);
    }
}