﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HW22_23
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>();

            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(10);
                employees.Add(Employee.RandEmployee());
            }
            
            Presenter presenter = new Presenter();

            presenter.Start(employees);

        }
    }
}
