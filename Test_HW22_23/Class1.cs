﻿using NUnit.Framework;
using HW22_23;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using NSubstitute;

namespace Test_HW22_23
{
    [TestFixture]
    public class CollctionManagerTest
    {

        public List<Person> _collexpected;
        public List<Person> _testcollection;
        [SetUp]

        public void CollectionCreate()
        {
            var testcollection = new List<Person> { new Person("Tom", "Robinson", 30, 1500),
            new Person("Mike", "Bawerman", 25, 1780),
            new Person("Liam", "Charlson", 22, 1130),
            new Person("Noah", "Longman", 26, 1540),
            new Person("Mason", "Holiday", 31, 1520),
            new Person("Logan", "Archibald", 34, 1390),
            new Person("Lucas", "Leapman", 38, 1470),
            new Person("Oliver", "Stevenson", 21, 4500),
            new Person("Jacob", "Thomas", 24, 4700),
            new Person("Elijah", "Walls", 35, 2300),
            new Person("Alexander", "Sheldon", 33, 5400),
            new Person("Henry", "WilKinson", 34, 1900),
            new Person("Carter", "Turner", 25, 2000),
            new Person("Gabriel", "Young", 20, 2500)};
            _testcollection = testcollection;

        }


        [Test]
        public void TestAdd()
        {
            var persadd = new Person("A", "D", 21, 200);
            var n = new HW22_23.CollectionManager<Person>(_testcollection);

            var result = n.Add(persadd);

            Assert.AreEqual(result, _testcollection.Take(5));
        }
        [Test]
        public void TestGetPage()
        {
            var n = new HW22_23.CollectionManager<Person>(_testcollection);
            //var cm = NSubstitute.Substitute.For<ICollectionManager<Person>>();
            //cm.FilterSorting(Arg.Any<Person>()., Arg.Any())
            var result = n.GetPage();

            Assert.AreEqual(result, _testcollection.Take(5));

            //stub.Divide(Arg.Any<int>(), Arg.Any<int>()).Returns(8);
            //var c = new Calculator.Calculator(stub);
        }
        [Test]
        public void TestNextPage()
        {
            var n = new HW22_23.CollectionManager<Person>(_testcollection);
            var result = n.NextPage();

            Assert.AreEqual(result, _testcollection.Skip(5).Take(5));

        }
        [Test]
        public void TestPreviousPage()
        {
            var n = new HW22_23.CollectionManager<Person>(_testcollection);
            var result = n.PreviousPage();

            Assert.AreEqual(result, _testcollection.Take(5));

        }
        [Test]
        public void TestOpenPageByNumber()
        {
            var n = new HW22_23.CollectionManager<Person>(_testcollection);
            var result = n.OpenPageByNumber(0);

            Assert.AreEqual(result, _testcollection.Take(5));

        }



    }
}
