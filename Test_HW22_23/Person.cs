﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_HW22_23
{
    public class Person
    {
        public string Name;
        public string SurName;
        public int Age;
        public int Solary;
        

        public Person() 
        {
        }

        public Person(string name, string surname, int age, int solary)
        {
            Name = name;
            SurName = surname;
            Age = age;
            Solary = solary;
        }
    }
}
